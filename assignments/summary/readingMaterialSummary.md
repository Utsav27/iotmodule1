## _INDUSTRY 3.0_
>![](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/2020-07-28__2_.png)

####  **FIELD DEVICES**   
>##### Field devices are either sensors or actuators in the factory, either they are measuring or actuating some process. They are connected in the factory itself.  
#### **Control Devices**
> #### These are either PC's, PLC's or DCS.These devices control field devices.
#### **Station**
> #### Set of field devices and control devices is called station.
#### **Work Centers**
> #### Set of stations are work centers, this is like one factory.
#### **Enterprise**
> #### Factories together make one enterprise.

## _INDUSTRY 3.0 ARCHITECTURE_
>![](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/2.png)
#### **FIELDBUS**
> #### Sensors and actuators communicate with each other through fieldbus. These devices are **Field Devices**
#### **SCADA AND ERP**
> #### Scada is on **work center level** and Erp is on **Enterprise level**.These are softwares which stores data(files and excel sheets).

 # _INDUSTRY 4.0 IS JUST THE INDUSTRY 3.0 CONNECTED TO THE INTERNET._
 ## What can you do with the data gathered from devices?
 >![](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/3.png)

 ## INDUSTRY 4.0 ARTICHECTURE
 ![](https://gitlab.com/Utsav27/iotmodule1/-/raw/master/assignments/summary/images/4.png)

 #### What does Edge Does?
 > #### Edge takes data from the controllers and convert it to the protocols that the cloud understands and sends the data from controller to cloud.

 # INDUSTRY 4.0 COMMUNICATION PROTOCOLS
 * _Mqtt_
 * _CoAP_
 * _AMQP_
 * _HTTP_
 * _WebSockets_
 * _RESTful API_

 # PROBLEMS WITH INDUSTRY 4.0 UPGRADES
 * _Cost_
 * _Downtime_
 * _Reliability_

 # CHALLENGES IN CONVERSION INDUSTRY 3.0 TO INDUSTRY 4.0
 * _Expensive Hardware_
 * _Lack of documentation_
 * _Properitary PLC protocols_

 # HOW TO MAKE YOUR OWN INDUSTRIAL IOT PRODUCT
 1. Identify most popular Industry 3.0 devices
 2. Study Protocols that these device communicates
 3. Get Data from Industry 3.0 devices
 4. Send them to cloud

## THE DATA YOU SEND TO THE INTERNET IS STORED IN THE _TSDB's_ 
> #### TSDB'S are time series databases i.e the value of your data is stored with respect to time.

## IOT TSDB TOOLS ARE 
* _Prometheus_
* _InfluxDB_
* _Grafana_
* _Thingsboard_
## IOT PLATFORMS
> #### IOT platforms helps to analyze the data, predective maintainence.

#### Examples of IOT Platforms
* _Google IOT_
* _AWS IOt_ 
* _Azure IOT_
* _Thingsboard_

## GET ALERTS
> #### Monitor TSDB and Trigger Alerts. **_Zapier_** and **_Twilio_** are tools that helps to raise alerts.





